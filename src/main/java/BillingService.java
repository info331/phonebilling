import java.util.ArrayList;

        public class BillingService {

            private ArrayList<PhoneCall> allCalls;

            public BillingService() {
                FileBasedCallJournal journal = new FileBasedCallJournal();
                allCalls = journal.ReadCallFile();
            }

            public void BillNumber(String phoneNumber) {
                int totalMinutes = 0;

                for (PhoneCall call : allCalls) {
                    if (phoneNumber.equals(call.getSourceNumber())) {
                        totalMinutes += call.getMinutes();
                    }
                }

                int totalAmount = totalMinutes * 10;

                StringBuilder customerReminder = new StringBuilder();
                customerReminder.append("Hi, this is a kind reminder for the owner of " + phoneNumber);
                customerReminder.append("Your balance by the end of the month is " + totalAmount);

                SmsSender.SendSms(phoneNumber, customerReminder.toString());
    }
}
