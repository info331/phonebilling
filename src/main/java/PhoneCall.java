
public class PhoneCall {
    private String sourceNumber;
    private String destinationNumber;
    private int minutes;

    public String getSourceNumber() {
        return sourceNumber;
    }

    public String getDestinationNumber() {
        return destinationNumber;
    }

    public int getMinutes() {
        return minutes;
    }
}
